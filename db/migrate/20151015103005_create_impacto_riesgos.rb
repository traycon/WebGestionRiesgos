class CreateImpactoRiesgos < ActiveRecord::Migration
  def change
    create_table :impacto_riesgos do |t|
      t.string :nombre
      t.integer :idUsuario

      t.timestamps null: false
    end
  end
end
