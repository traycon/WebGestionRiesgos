class CreateTipoProyectos < ActiveRecord::Migration
  def change
    create_table :tipo_proyectos do |t|
      t.string :nombre
      t.integer :idUsuario

      t.timestamps null: false
    end
  end
end
