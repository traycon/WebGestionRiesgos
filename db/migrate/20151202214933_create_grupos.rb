class CreateGrupos < ActiveRecord::Migration
  def change
    create_table :grupos do |t|
      t.integer :idUsuario
      t.integer :idGrupoUsuario

      t.timestamps null: false
    end
  end
end
