class CreateProyectos < ActiveRecord::Migration
  def change
    create_table :proyectos do |t|
      t.string :nombre
      t.string :tipo
      t.text :descripcion
      t.date :inicio
      t.date :fin
      t.integer :idUsuario

      t.timestamps null: false
    end
  end
end
