class CreateRiesgos < ActiveRecord::Migration
  def change
    create_table :riesgos do |t|
      t.string :nombre
      t.string :categoria
      t.string :tipo
      t.string :impacto
      t.string :probabilidad
      t.text :descripcion
      t.text :factores
      t.text :reduccion
      t.text :supervision
      t.integer :idProyecto
      t.integer :idUsuario

      t.timestamps null: false
    end
  end
end
