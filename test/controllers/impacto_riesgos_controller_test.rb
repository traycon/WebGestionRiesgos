require 'test_helper'

class ImpactoRiesgosControllerTest < ActionController::TestCase
  setup do
    @impacto_riesgo = impacto_riesgos(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:impacto_riesgos)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create impacto_riesgo" do
    assert_difference('ImpactoRiesgo.count') do
      post :create, impacto_riesgo: { idUsuario: @impacto_riesgo.idUsuario, nombre: @impacto_riesgo.nombre }
    end

    assert_redirected_to impacto_riesgo_path(assigns(:impacto_riesgo))
  end

  test "should show impacto_riesgo" do
    get :show, id: @impacto_riesgo
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @impacto_riesgo
    assert_response :success
  end

  test "should update impacto_riesgo" do
    patch :update, id: @impacto_riesgo, impacto_riesgo: { idUsuario: @impacto_riesgo.idUsuario, nombre: @impacto_riesgo.nombre }
    assert_redirected_to impacto_riesgo_path(assigns(:impacto_riesgo))
  end

  test "should destroy impacto_riesgo" do
    assert_difference('ImpactoRiesgo.count', -1) do
      delete :destroy, id: @impacto_riesgo
    end

    assert_redirected_to impacto_riesgos_path
  end
end
