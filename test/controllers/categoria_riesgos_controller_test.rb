require 'test_helper'

class CategoriaRiesgosControllerTest < ActionController::TestCase
  setup do
    @categoria_riesgo = categoria_riesgos(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:categoria_riesgos)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create categoria_riesgo" do
    assert_difference('CategoriaRiesgo.count') do
      post :create, categoria_riesgo: { idUsuario: @categoria_riesgo.idUsuario, nombre: @categoria_riesgo.nombre }
    end

    assert_redirected_to categoria_riesgo_path(assigns(:categoria_riesgo))
  end

  test "should show categoria_riesgo" do
    get :show, id: @categoria_riesgo
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @categoria_riesgo
    assert_response :success
  end

  test "should update categoria_riesgo" do
    patch :update, id: @categoria_riesgo, categoria_riesgo: { idUsuario: @categoria_riesgo.idUsuario, nombre: @categoria_riesgo.nombre }
    assert_redirected_to categoria_riesgo_path(assigns(:categoria_riesgo))
  end

  test "should destroy categoria_riesgo" do
    assert_difference('CategoriaRiesgo.count', -1) do
      delete :destroy, id: @categoria_riesgo
    end

    assert_redirected_to categoria_riesgos_path
  end
end
