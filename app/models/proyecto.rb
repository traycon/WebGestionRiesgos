class Proyecto < ActiveRecord::Base
  
  validates :nombre ,:tipo , :descripcion ,presence:true
  
  validates :nombre, length: { in: 1..255 , message: "debe tener entre 1 y 255 caracteres"}
  validates :tipo, length: { in: 1..255 , message: "debe tener entre 1 y 255 caracteres"}
  validates :descripcion, length: { in: 1..255 , message: "debe tener entre 1 y 255 caracteres"}
end
