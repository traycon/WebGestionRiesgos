class CategoriaRiesgo < ActiveRecord::Base
  validates :nombre, presence: true
  validates :nombre, length: { in: 1..255 , message: "debe tener entre 1 y 255 caracteres"}
end
