class Usuario < ActiveRecord::Base


  validates :email ,:username ,:alias , :password , presence: true 
  # Validamos en una expresion regular nuestro email
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates :email, format: { :with => VALID_EMAIL_REGEX , message: "El formato del correo es invalido" }

  validates :username, length: { in: 3..32 , message: "debe tener entre 3 y 32 caracteres"}
  validates :username, uniqueness: {case_sensitive: false ,message: "ya esta registrado"}
  validates :alias, length: { in: 1..255 , message: "debe tener entre 1 y 255 caracteres"}
  validates :password, length: { in: 8..32 , message: "debe tener entre 8 y 32 caracteres"}
  # Validamos que el email sea unico
  validates :email, uniqueness: {case_sensitive: false ,message: "ya esta registrado"}
end
