module SessionsHelper

  def log_in(user)
    session[:user_id] = user
  end
  def current_user
    @current_user =session[:user_id]
  end
  def log_out
    session.delete(:user_id)
    @current_user = nil
  end
  def proyecto(id)
    session[:proyect_id] = id
  end
  def current_proyecto
    @current_proyecto=session[:proyect_id]
  end
    def grupo(id)
    session[:grupo_id] = id
  end
  def current_grupo
    @current_grupo=session[:grupo_id]
  end
end