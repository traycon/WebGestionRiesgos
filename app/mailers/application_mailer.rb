class ApplicationMailer < ActionMailer::Base
  default from: "centerbot0002@gmail.com"
  layout 'mailer'
end
