/*
	Prologue by HTML5 UP
	html5up.net | @n33co
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
*/

(function($) {

	skel.breakpoints({
		wide: '(min-width: 961px) and (max-width: 1880px)',
		normal: '(min-width: 961px) and (max-width: 1620px)',
		narrow: '(min-width: 961px) and (max-width: 1320px)',
		narrower: '(max-width: 960px)',
		mobile: '(max-width: 736px)'
	});

	$(function() {

		var	$window = $(window),
			$body = $('body');

		// Disable animations/transitions until the page has loaded.
			$body.addClass('is-loading');

			$window.on('load', function() {
				$body.removeClass('is-loading');
			});

		// CSS polyfills (IE<9).
			if (skel.vars.IEVersion < 9)
				$(':last-child').addClass('last-child');

		// Fix: Placeholder polyfill.
			$('form').placeholder();

		// Prioritize "important" elements on mobile.
			skel.on('+mobile -mobile', function() {
				$.prioritize(
					'.important\\28 mobile\\29',
					skel.breakpoint('mobile').active
				);
			});

		// Scrolly links.
			$('.scrolly').scrolly();

		// Nav.
			var $nav_a = $('#nav a');

			// Scrolly-fy links.
				$nav_a
					.scrolly()
					.on('click', function(e) {

						var t = $(this),
							href = t.attr('href');

						if (href[0] != '#')
							return;

						e.preventDefault();

						// Clear active and lock scrollzer until scrolling has stopped
							$nav_a
								.removeClass('active')
								.addClass('scrollzer-locked');

						// Set this link to active
							t.addClass('active');

					});

			// Initialize scrollzer.
				var ids = [];

				$nav_a.each(function() {

					var href = $(this).attr('href');

					if (href[0] != '#')
						return;

					ids.push(href.substring(1));

				});

				$.scrollzer(ids, { pad: 200, lastHack: true });

		// Header (narrower + mobile).

			// Toggle.
				$(
					'<div id="headerToggle">' +
						'<a href="#header" class="toggle"></a>' +
					'</div>'
				)
					.appendTo($body);

			// Header.
				$('#header')
					.panel({
						delay: 500,
						hideOnClick: true,
						hideOnSwipe: true,
						resetScroll: true,
						resetForms: true,
						side: 'left',
						target: $body,
						visibleClass: 'header-visible'
					});

			// Fix: Remove transitions on WP<10 (poor/buggy performance).
				if (skel.vars.os == 'wp' && skel.vars.osVersion < 10)
					$('#headerToggle, #header, #main')
						.css('transition', 'none');

	});

})(jQuery);

//Validacion login
function notNull(elemento) {
 	var Nombre=elemento.value;
	
   if(!Nombre || /^\s*$/.test(Nombre) ||Nombre=="" || Nombre=="Seleccione un tipo" || Nombre=="Seleccione una categoria" || Nombre=="Seleccione un impacto"){
   	elemento.style.borderColor='red';
   	return false;
   }else{
		elemento.style.borderColor='green';
		return true;
	}
  
}

function validarEmail(mail){
  	var email=mail.value;
     expr = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if(!email || /^\s*$/.test(email) ||email==""){
    	mail.style.borderColor='red';
    	return false;
    }else if ( !expr.test(email) ){
	mail.style.borderColor='red';
	return false;
	}else{
		mail.style.borderColor='green';
		return true;
}
  	
}


var contrasena;
var recontrasena;
function validarContrasena(pass){
	var digitos=0;
	var otros=0;
	contrasena=pass.value;
	for ( var i=0; i<contrasena.length;i++) {
		if(contrasena.charAt(i)>='0' && contrasena.charAt(i)<='9'){
			digitos++;
		}else{
			otros++;
		}
	}
	if(digitos>=2&&otros>=2&&digitos+otros>=8&&digitos+otros<=32){
		pass.style.borderColor='green';
		return true;
	}else{
		pass.style.borderColor='red';
		return false;
	}
	
}

function igualPass(pass,pass2){
	if(pass.length==pass2.length){
	for ( var i=0; i<pass.length;i++) {
		if(pass.charAt(i)!=pass2.charAt(i)){
			return false;
		}
	}
	return true;
	}else{
		return false;
	}
}

function validacionRegistro() {
	var nombre=document.getElementById("usuario_username");
	var alias=document.getElementById("usuario_alias");
	var email=document.getElementById("usuario_email");
	var contrasena=document.getElementById("usuario_password");
	var contrasena2=document.getElementById("usuario_password2");
	if(notNull(nombre)&&notNull(alias)&&validarEmail(email)&&validarContrasena(contrasena)&&validarContrasena(contrasena2)&&igualPass(contrasena,contrasena2)){
		return true;
	}else{
		return false;
	}
	
}

function validacionRegistro1() {
	var alias=document.getElementById("usuario_alias");
	var email=document.getElementById("usuario_email");
	var contrasena=document.getElementById("usuario_password");
	var contrasena2=document.getElementById("usuario_password2");
	if(notNull(alias)&&validarEmail(email)&&validarContrasena(contrasena)&&validarContrasena(contrasena2)&&igualPass(contrasena,contrasena2)){
		return true;
	}else{
		return false;
	}
	
}

function validacionProyecto() {
	var nombre=document.getElementById("proyecto_nombre");
	var descr=document.getElementById("proyecto_descripcion");
	var tipo=document.getElementById("proyecto_tipo");
	var fechaIni= new Date(document.getElementById("proyecto_inicio_1i").value, document.getElementById("proyecto_inicio_2i").value, document.getElementById("proyecto_inicio_3i").value);
	var fechaFin=new Date(document.getElementById("proyecto_fin_1i").value, document.getElementById("proyecto_fin_2i").value, document.getElementById("proyecto_fin_3i").value);
	if(notNull(descr)&&notNull(nombre)&&notNull(tipo)&&fechaIni<=fechaFin){
		return true;
	}else{
		return false;
	}
	
}

function validacionRiesgo() {
	var nombre=document.getElementById("riesgo_nombre");
	var tipo=document.getElementById("riesgo_tipo");
	var categoria= document.getElementById("riesgo_categoria");
	var impacto=document.getElementById("riesgo_impacto");
	var factores=document.getElementById("riesgo_factores");
	var reduccion=document.getElementById("riesgo_reduccion");
	if(notNull(nombre)&&notNull(tipo)&&notNull(categoria)&&notNull(impacto)&&notNull(factores)&&notNull(reduccion)){
		return true;
	}else{
		return false;
	}
	
}

function validacionGrupo(){
	var nombre=document.getElementById("grupo_nombre");
	if(notNull(nombre)){
		return true;
	}else{
		return false;
	}
}
