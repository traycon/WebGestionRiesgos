class RiesgosController < ApplicationController
before_action :require_login
  before_action :set_riesgo, only: [:show, :edit, :update, :destroy, :add]

  def require_login
     current_user
    unless @current_user!=nil
      redirect_to root_path
    end
  end
  # GET /riesgos
  # GET /riesgos.json
  def index
    current_user
    current_proyecto
    @riesgos = Riesgo.all
        @impacto_riesgos = ImpactoRiesgo.all
    @categoria_riesgos = CategoriaRiesgo.all
    
  end

  # GET /riesgos/1
  # GET /riesgos/1.json
  def show
    current_user
    current_proyecto
        @impacto_riesgos = ImpactoRiesgo.all
    @categoria_riesgos = CategoriaRiesgo.all
    @lineaCorte=Proyecto.find(@current_proyecto).lineaCorte
  end

  # GET /riesgos/new
  def new
    current_user
    current_proyecto
    @impacto_riesgos = ImpactoRiesgo.all
    @categoria_riesgos = CategoriaRiesgo.all
    @riesgo = Riesgo.new
  end

  # GET /riesgos/1/edit
  def edit
    current_user
    current_proyecto
    @impacto_riesgos = ImpactoRiesgo.all
    @categoria_riesgos = CategoriaRiesgo.all
  end

  # POST /riesgos
  # POST /riesgos.json
  def create
        @impacto_riesgos = ImpactoRiesgo.all
    @categoria_riesgos = CategoriaRiesgo.all
    current_user
    current_proyecto
    @riesgo = Riesgo.new(riesgo_params)
    @riesgo.idUsuario= Proyecto.find(@current_proyecto).idUsuario
    @riesgo.idProyecto=@current_proyecto
    if numeric(@riesgo.impacto)
    @riesgo.impacto=ImpactoRiesgo.find(@riesgo.impacto).nombre
    end
    if numeric(@riesgo.categoria)
    @riesgo.categoria=CategoriaRiesgo.find(@riesgo.categoria).nombre
    end
    respond_to do |format|
      if @riesgo.save
        format.html { redirect_to @riesgo, notice: '' }
        format.json { render :show, status: :created, location: @riesgo }
      else
        format.html { render :new }
        format.json { render json: @riesgo.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /riesgos/1
  # PATCH/PUT /riesgos/1.json
  def numeric(s)
    Float(s) != nil rescue false
  end
  def update
        @impacto_riesgos = ImpactoRiesgo.all
    @categoria_riesgos = CategoriaRiesgo.all
    current_user
    current_proyecto
    respond_to do |format|
      riesgo=riesgo_params
      if numeric(riesgo[:impacto])
   riesgo[:impacto]=ImpactoRiesgo.find(riesgo_params[:impacto])[:nombre]
   end
   if numeric(riesgo[:categoria])
   riesgo[:categoria]=CategoriaRiesgo.find(riesgo_params[:categoria])[:nombre]
   end
      if @riesgo.update(riesgo)
        format.html { redirect_to @riesgo, notice: '' }
        format.json { render :show, status: :ok, location: @riesgo }
      else
        format.html { render :edit }
        format.json { render json: @riesgo.errors, status: :unprocessable_entity }
      end
    end
  end

def add
  current_user
    current_proyecto
    riesgo = Riesgo.new
    riesgo.nombre=@riesgo.nombre
    riesgo.categoria=@riesgo.categoria
    riesgo.impacto=@riesgo.impacto
    riesgo.tipo=@riesgo.tipo
    riesgo.probabilidad=@riesgo.probabilidad
    riesgo.descripcion=@riesgo.descripcion
    riesgo.factores=@riesgo.factores
    riesgo.reduccion=@riesgo.reduccion
    riesgo.supervision=@riesgo.supervision
    riesgo.idUsuario= @riesgo.idUsuario
    riesgo.idProyecto=@current_proyecto
    respond_to do |format|
      if riesgo.save
        format.html { redirect_to riesgo, notice: '' }
        format.json { render :show, status: :created, location: riesgo }
      else
        format.html { render :new }
        format.json { render json: riesgo.errors, status: :unprocessable_entity }
      end
    end
end
  # DELETE /riesgos/1
  # DELETE /riesgos/1.json
  def destroy
        @impacto_riesgos = ImpactoRiesgo.all
    @categoria_riesgos = CategoriaRiesgo.all
    current_user
    current_proyecto
    @riesgo.destroy

     @proyec=Proyecto.find(@current_proyecto)
     if @proyec.lineaCorte>Riesgo.all.where(:idUsuario=>@proyec.idUsuario).where(:idProyecto=>@proyec.id).count
    @proyec.lineaCorte=@proyec.lineaCorte.to_i-1
    @proyec.save   
      end

    respond_to do |format|
      format.html { redirect_to proyecto_path(@current_proyecto), notice: '' }
      format.json { head :no_content }
    end
  end
 
  private
    # Use callbacks to share common setup or constraints between actions.
    def set_riesgo

      current_user
      if !numeric(params[:id])
        redirect_to root_path
      else
        aux=0
        Grupo.all.where(:idUsuario=> @current_user["id"]).each do |grupo|
        if Riesgo.all.where(:idUsuario=>grupo.idGrupoUsuario).where(:id=>params[:id]).count>0
       @riesgo = Riesgo.find(params[:id])
          aux=1
      end
      end
      if aux==0
        redirect_to root_path
      end
 end
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def riesgo_params
      params.require(:riesgo).permit(:nombre, :categoria, :tipo, :impacto, :probabilidad, :descripcion, :factores, :reduccion, :supervision, :idProyecto, :idUsuario)
    end
end
