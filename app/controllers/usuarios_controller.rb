class UsuariosController < ApplicationController
 before_action :require_login, only:[:show, :edit, :update, :destroy]
  before_action :set_usuario, only: [:show, :edit, :update, :destroy]
 
  def require_login
     current_user
    unless @current_user!=nil
      redirect_to root_path
    end
  end
  # GET /usuarios
  # GET /usuarios.json
  def index
    redirect_to root_path
    @usuarios = Usuario.all
  end

  # GET /usuarios/1
  # GET /usuarios/1.json
  def show
    current_user
  end

  # GET /usuarios/new
  def new
    @usuario = Usuario.new
  end

  # GET /usuarios/1/edit
  def edit
    current_user
  end

  # POST /usuarios
  # POST /usuarios.json
  def create
    current_user
    @usuario = Usuario.new(usuario_params)

    respond_to do |format|
      if @usuario.save
        format.html { redirect_to root_path, notice: '' }
        format.json { render :show, status: :created, location: @usuario }
      else
        format.html { render :new }
        format.json { render json: @usuario.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /usuarios/1
  # PATCH/PUT /usuarios/1.json
  def update
    current_user
    userp=usuario_params
    userp[:username]=@current_user["username"]
    respond_to do |format|
      if @usuario.update(userp)
        format.html { redirect_to @usuario, notice: '' }
        format.json { render :show, status: :ok, location: @usuario }
      else
        format.html { render :edit }
        format.json { render json: @usuario.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /usuarios/1
  # DELETE /usuarios/1.json
  def destroy
   current_user
    @usuario.destroy
    respond_to do |format|
      format.html { redirect_to usuarios_url, notice: '' }
      format.json { head :no_content }
    end
  end
  

  private
    def numeric(s)
    Float(s) != nil rescue false
  end
    # Use callbacks to share common setup or constraints between actions.
    def set_usuario
      current_user
      if !numeric(params[:id])
        redirect_to root_path
      else
        if @current_user["id"]!=Float(params[:id])
          redirect_to root_path
        else
      @usuario = Usuario.find(params[:id])
      end
      end
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def usuario_params
      params.require(:usuario).permit(:username, :password, :alias, :email)
    end
end
