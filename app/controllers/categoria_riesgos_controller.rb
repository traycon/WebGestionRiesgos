class CategoriaRiesgosController < ApplicationController
  before_action :set_categoria_riesgo, only: [:show, :edit, :update, :destroy]
  before_action :require_login
  def require_login
     current_user
    unless @current_user!=nil
      redirect_to root_path
    end
  end
layout false
  # GET /categoria_riesgos
  # GET /categoria_riesgos.json
  def index
    current_user
    @categoria_riesgos = CategoriaRiesgo.all
  end

  # GET /categoria_riesgos/1
  # GET /categoria_riesgos/1.json
  def show
    current_user
  end

  # GET /categoria_riesgos/new
  def new
    current_user
    @categoria_riesgo = CategoriaRiesgo.new
  end

  # GET /categoria_riesgos/1/edit
  def edit
    current_user
  end

  # POST /categoria_riesgos
  # POST /categoria_riesgos.json
  def create
    current_user
    @categoria_riesgo = CategoriaRiesgo.new(categoria_riesgo_params)
    current_proyecto
    @categoria_riesgo.idUsuario=Proyecto.find(@current_proyecto).idUsuario
    respond_to do |format|
      if @categoria_riesgo.save
        format.html { redirect_to new_categoria_riesgo_path}
        format.json { render :show, status: :created, location: @categoria_riesgo }
      else
        format.html { render :new }
        format.json { render json: @categoria_riesgo.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /categoria_riesgos/1
  # PATCH/PUT /categoria_riesgos/1.json
  def update
    current_user
    respond_to do |format|
      if @categoria_riesgo.update(categoria_riesgo_params)
        format.html { redirect_to @categoria_riesgo, notice: '' }
        format.json { render :show, status: :ok, location: @categoria_riesgo }
      else
        format.html { render :edit }
        format.json { render json: @categoria_riesgo.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /categoria_riesgos/1
  # DELETE /categoria_riesgos/1.json
  def destroy
    @categoria_riesgo.destroy
    current_user
    respond_to do |format|
      format.html { redirect_to categoria_riesgos_url, notice: '' }
      format.json { head :no_content }
    end
  end
  
 def numeric(s)
    Float(s) != nil rescue false
  end
  
  private
    # Use callbacks to share common setup or constraints between actions.
    def set_categoria_riesgo
      
       current_user
      if !numeric(params[:id])
        redirect_to root_path
      else
           aux=0
        Grupo.all.where(:idUsuario=> @current_user["id"]).each do |grupo|
        if CategoriaRiesgo.all.where(:idUsuario=>grupo.idGrupoUsuario).where(:id=>params[:id]).count>0
          @categoria_riesgo = CategoriaRiesgo.find(params[:id])
          aux=1
      end
      end
      if aux==0
        redirect_to root_path
      end
     end
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def categoria_riesgo_params
      params.require(:categoria_riesgo).permit(:nombre, :idUsuario)
    end
end
