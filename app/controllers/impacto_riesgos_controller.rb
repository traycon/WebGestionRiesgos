class ImpactoRiesgosController < ApplicationController
    before_action :require_login
  before_action :set_impacto_riesgo, only: [:show, :edit, :update, :destroy]
  
  def require_login
     current_user
    unless @current_user!=nil
      redirect_to root_path
    end
  end
layout false
  # GET /impacto_riesgos
  # GET /impacto_riesgos.json
  def index
    current_user
    @impacto_riesgos = ImpactoRiesgo.all
  end

  # GET /impacto_riesgos/1
  # GET /impacto_riesgos/1.json
  def show
    current_user
  end

  # GET /impacto_riesgos/new
  def new
    current_user
    @impacto_riesgo = ImpactoRiesgo.new
  end

  # GET /impacto_riesgos/1/edit
  def edit
    current_user
  end

  # POST /impacto_riesgos
  # POST /impacto_riesgos.json
   def numeric(s)
    Float(s) != nil rescue false
  end
  def create
    current_user
    @impacto_riesgo = ImpactoRiesgo.new(impacto_riesgo_params)
    current_proyecto
    @impacto_riesgo.idUsuario=Proyecto.find(@current_proyecto).idUsuario
        if !numeric(@impacto_riesgo.ponderacion)
    @impacto_riesgo.ponderacion=0
    end
    respond_to do |format|
      if @impacto_riesgo.save
        format.html { redirect_to new_impacto_riesgo_path }
        format.json { render :show, status: :created, location: @impacto_riesgo }
      else
        format.html { render :new }
        format.json { render json: @impacto_riesgo.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /impacto_riesgos/1
  # PATCH/PUT /impacto_riesgos/1.json
  def update
    current_user
    impacto_riesgo=impacto_riesgo_params
      if !numeric(impacto_riesgo[:ponderacion])
   impacto_riesgo[:ponderacion]=0
   end
    respond_to do |format|
      if @impacto_riesgo.update(impacto_riesgo)
        format.html { redirect_to @impacto_riesgo, notice: '' }
        format.json { render :show, status: :ok, location: @impacto_riesgo }
      else
        format.html { render :edit }
        format.json { render json: @impacto_riesgo.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /impacto_riesgos/1
  # DELETE /impacto_riesgos/1.json
  def destroy
    current_user
    @impacto_riesgo.destroy
    respond_to do |format|
      format.html { redirect_to impacto_riesgos_url, notice: '' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_impacto_riesgo
      
      current_user
      if !numeric(params[:id])
        redirect_to root_path
      else
          aux=0
        Grupo.all.where(:idUsuario=> @current_user["id"]).each do |grupo|
        if ImpactoRiesgo.all.where(:idUsuario=>grupo.idGrupoUsuario).where(:id=>params[:id]).count>0
       @impacto_riesgo = ImpactoRiesgo.find(params[:id])
          aux=1
      end
      end
      if aux==0
        redirect_to root_path
      end
      end
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def impacto_riesgo_params
      params.require(:impacto_riesgo).permit(:nombre, :idUsuario, :ponderacion)
    end
end
