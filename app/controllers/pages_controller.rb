class PagesController < ApplicationController
    before_action :require_login,only: [:estadisticas,:addUserToGroup,:addtog,:darPermisos,:quitarPermisos]
  def require_login
     current_user
    unless @current_user!=nil
      redirect_to root_path
    end
  end
  
  def home
    current_user
  end
  
  def login
    current_user
     user = Usuario.find_by(username: params[:usuario][:usernameLogin])
    if user == nil
      @error="Información incorrecta"
      @user=Usuario.new
    else
    if user.password.eql? params[:usuario][:passwordLogin]
      log_in user
      current_user
        redirect_to root_path
     
    else
      @error="Información incorrecta"
      @user=Usuario.new
    end
  end
  end
  
  def salir
    current_user
    log_out
    redirect_to root_path
  end
  
  def recuperarPass
    current_user
    if Usuario.all.where(:email=>params[:email]).count>0
    user=Usuario.find_by(email: params[:email])
    UserMailer.recuperar_email(user).deliver
    redirect_to correo_path
    end
  end
  def recuperar
    current_user
  end
  def correo
    current_user
  end
  def estadisticas
    current_user
  end
    def numeric(s)
    Float(s) != nil rescue false
  end
  def addUserToGroup
    current_user
      if !numeric(params[:id])
        redirect_to proyectos_path
      else
        if Grupo.all.where(:idUsuario=> @current_user["id"]).where(:idGrupoUsuario=>params[:id]).count>0
          grupo(params[:id])
        else
          redirect_to proyectos_path
        end
      end
  end
  
  def addtog
    current_user
    current_grupo
    if Usuario.all.where(:email=>params[:email]).count>0
    user=Usuario.find_by(email: params[:email])
    if Grupo.all.where(:idGrupoUsuario=>@current_grupo).where(:idUsuario=>user.id).count<1
    gru=Grupo.new
    gru.idUsuario=user.id
    gru.idGrupoUsuario=@current_grupo
    gru.admin="false"
    gru.nombre=Grupo.all.where(:idGrupoUsuario=>@current_grupo).where(:idUsuario=>@current_user["id"]).first.nombre
    gru.save
    redirect_to proyectos_path
    end
    end
  end
  
  def darPermisos
    current_user
      if !numeric(params[:igu]) and !numeric(params[:iu]) 
        redirect_to proyectos_path
      else
        if Grupo.all.where(:idUsuario=> @current_user["id"]).where(:idGrupoUsuario=>params[:igu]).count>0 and Grupo.all.where(:idUsuario=> params[:iu]).where(:idGrupoUsuario=>params[:igu]).count>0 
          if Grupo.all.where(:idUsuario=> @current_user["id"]).where(:idGrupoUsuario=>params[:igu]).first.admin=="true" or Grupo.all.where(:idUsuario=> @current_user["id"]).where(:idGrupoUsuario=>params[:igu]).first.admin=="tru"
            grup=Grupo.all.where(:idUsuario=> params[:iu]).where(:idGrupoUsuario=>params[:igu]).first
            grup.admin="tru"
            grup.save
            redirect_to proyectos_path
          else
            redirect_to proyectos_path
          end
        else
          redirect_to proyectos_path
        end
      end
  end
  
  
    def quitarPermisos
    current_user
      if !numeric(params[:igu]) and !numeric(params[:iu]) 
        redirect_to proyectos_path
      else
        if Grupo.all.where(:idUsuario=> @current_user["id"]).where(:idGrupoUsuario=>params[:igu]).count>0 and Grupo.all.where(:idUsuario=> params[:iu]).where(:idGrupoUsuario=>params[:igu]).count>0
          if Grupo.all.where(:idUsuario=> @current_user["id"]).where(:idGrupoUsuario=>params[:igu]).first.admin=="true"
            grup=Grupo.all.where(:idUsuario=> params[:iu]).where(:idGrupoUsuario=>params[:igu]).first
            grup.admin="false"
            grup.save
            redirect_to proyectos_path
          else
            redirect_to proyectos_path
          end
        else
          redirect_to proyectos_path
        end
      end
  end
  
  def del_user_grupo
    current_user
    if !numeric(params[:igu]) and !numeric(params[:iu]) 
        redirect_to proyectos_path
      else
        if Grupo.all.where(:idUsuario=> @current_user["id"]).where(:idGrupoUsuario=>params[:igu]).count>0 and Grupo.all.where(:idUsuario=> params[:iu]).where(:idGrupoUsuario=>params[:igu]).count>0
          if Grupo.all.where(:idUsuario=> @current_user["id"]).where(:idGrupoUsuario=>params[:igu]).first.admin=="true"
            Grupo.all.where(:idUsuario=> params[:iu]).where(:idGrupoUsuario=>params[:igu]).destroy_all
            redirect_to proyectos_path
          else
            redirect_to proyectos_path
          end
        else
          redirect_to proyectos_path
        end
      end
  end
  
end
