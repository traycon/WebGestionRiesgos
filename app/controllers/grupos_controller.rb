class GruposController < ApplicationController
  before_action :require_login
  before_action :set_grupo, only: [:show, :edit, :update]
    
  def require_login
     current_user
    unless @current_user!=nil
      redirect_to root_path
    end
  end
  # GET /grupos
  # GET /grupos.json
  def index
    current_user
    @grupos = Grupo.all
  end

  # GET /grupos/1
  # GET /grupos/1.json
  def show
    current_user
  end

  # GET /grupos/new
  def new
    @grupo = Grupo.new
    current_user
  end

  # GET /grupos/1/edit
  def edit
    current_user
  end

  # POST /grupos
  # POST /grupos.json
  def create
    current_user
    @grupo = Grupo.new(grupo_params)
    @grupo.idUsuario=@current_user["id"]
    @grupo.admin="true"
    grupo=@grupo
    respond_to do |format|
      if @grupo.save
        @grupo.idGrupoUsuario=@grupo.id
        @grupo.save
          tipoPro=TipoProyecto.new
        tipoPro.nombre="Software de Aplicaciones Web"
        tipoPro.idUsuario=grupo.idGrupoUsuario
        tipoPro.save
        impactoR=ImpactoRiesgo.new
        impactoR.nombre="Catastrófico"
        impactoR.idUsuario=grupo.idGrupoUsuario
        impactoR.ponderacion=100
        impactoR.save
         impactoR=ImpactoRiesgo.new
        impactoR.nombre="Crítico"
        impactoR.ponderacion=65
        impactoR.idUsuario=grupo.idGrupoUsuario
        impactoR.save
         impactoR=ImpactoRiesgo.new
        impactoR.nombre="Marginal"
        impactoR.ponderacion=35
        impactoR.idUsuario=grupo.idGrupoUsuario
        impactoR.save
         impactoR=ImpactoRiesgo.new
        impactoR.nombre="Despreciable"
        impactoR.ponderacion=10
        impactoR.idUsuario=grupo.idGrupoUsuario
        impactoR.save
        categoriaR=CategoriaRiesgo.new
        categoriaR.nombre="Planificación"
        categoriaR.idUsuario=grupo.idGrupoUsuario
        categoriaR.save
         categoriaR=CategoriaRiesgo.new
        categoriaR.nombre="Organización y gestión"
        categoriaR.idUsuario=grupo.idGrupoUsuario
        categoriaR.save
         categoriaR=CategoriaRiesgo.new
        categoriaR.nombre="Entorno de desarrollo"
        categoriaR.idUsuario=grupo.idGrupoUsuario
        categoriaR.save
         categoriaR=CategoriaRiesgo.new
        categoriaR.nombre="Usuarios finales"
        categoriaR.idUsuario=grupo.idGrupoUsuario
        categoriaR.save
         categoriaR=CategoriaRiesgo.new
        categoriaR.nombre="Cliente"
        categoriaR.idUsuario=grupo.idGrupoUsuario
        categoriaR.save
         categoriaR=CategoriaRiesgo.new
        categoriaR.nombre="Personal contratado"
        categoriaR.idUsuario=grupo.idGrupoUsuario
        categoriaR.save
         categoriaR=CategoriaRiesgo.new
        categoriaR.nombre="Requisitos"
        categoriaR.idUsuario=grupo.idGrupoUsuario
        categoriaR.save
         categoriaR=CategoriaRiesgo.new
        categoriaR.nombre="Producto"
        categoriaR.idUsuario=grupo.idGrupoUsuario
        categoriaR.save
        categoriaR=CategoriaRiesgo.new
        categoriaR.nombre="Fuerzas mayores"
        categoriaR.idUsuario=grupo.idGrupoUsuario
        categoriaR.save
        categoriaR=CategoriaRiesgo.new
        categoriaR.nombre="Personal"
        categoriaR.idUsuario=grupo.idGrupoUsuario
        categoriaR.save
        categoriaR=CategoriaRiesgo.new
        categoriaR.nombre="Diseño e implementación"
        categoriaR.idUsuario=grupo.idGrupoUsuario
        categoriaR.save
        categoriaR=CategoriaRiesgo.new
        categoriaR.nombre="Proceso"
        categoriaR.idUsuario=grupo.idGrupoUsuario
        categoriaR.save
        format.html { redirect_to proyectos_path, notice: '' }
        format.json { render :show, status: :created, location: @grupo }
      else
        format.html { render :new }
        format.json { render json: @grupo.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /grupos/1
  # PATCH/PUT /grupos/1.json
  def update
    respond_to do |format|
      if @grupo.update(grupo_params)
        format.html { redirect_to @grupo, notice: 'Grupo was successfully updated.' }
        format.json { render :show, status: :ok, location: @grupo }
      else
        format.html { render :edit }
        format.json { render json: @grupo.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /grupos/1
  # DELETE /grupos/1.json
  def destroy
    Grupo.all.where(:idGrupoUsuario=> params[:id]).destroy_all
    respond_to do |format|
      format.html { redirect_to proyectos_path, notice: '' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_grupo
      @grupo = Grupo.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def grupo_params
      params.require(:grupo).permit(:idUsuario, :idGrupoUsuario,:nombre)
    end
end
