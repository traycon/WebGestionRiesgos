class ProyectosController < ApplicationController
 before_action :require_login
  before_action :set_proyecto, only: [:show, :edit, :update, :destroy]

  def require_login
     current_user
    unless @current_user!=nil
      redirect_to root_path
    end
  end
  # GET /proyectos
  # GET /proyectos.json
  def index
    current_user
    current_grupo
    @proyectos = Proyecto.all
     @tipo_proyectos = TipoProyecto.all
  end

  # GET /proyectos/1
  # GET /proyectos/1.json
  def show
     @tipo_proyectos = TipoProyecto.all
    current_user
    current_grupo
    proyecto @proyecto.id
    current_proyecto
     @riesgos = Riesgo.all
     pr=Proyecto.find(@current_proyecto)
     @lineaCorte=pr.lineaCorte
  end
  
   def lineaCorteUp
    current_user
    current_proyecto
    @proyec=Proyecto.find(@current_proyecto)
    @proyec.lineaCorte=@proyec.lineaCorte.to_i-1
    @proyec.save
    current_grupo
    redirect_to @proyec
    
  end
  
  def lineaCorteDown
    current_user
    current_proyecto
     @proyec=Proyecto.find(@current_proyecto)
    @proyec.lineaCorte=@proyec.lineaCorte.to_i+1
    @proyec.save
    current_grupo
    redirect_to @proyec
  end
  
  def numeric(s)
    Float(s) != nil rescue false
  end
  # GET /proyectos/new
  def new
    current_user
    
    if !numeric(params[:id])
        redirect_to proyectos_path
      else
        if Grupo.all.where(:idUsuario=> @current_user["id"]).where(:idGrupoUsuario=>params[:id]).count>0
          grupo(params[:id])
          current_grupo
          @proyecto = Proyecto.new
          @tipo_proyectos = TipoProyecto.all
      else
        redirect_to proyectos_path
      end
      end
    
  end

  # GET /proyectos/1/edit
  def edit
    current_user
    current_grupo
     @tipo_proyectos = TipoProyecto.all
  end

  # POST /proyectos
  # POST /proyectos.json
  def create
     @tipo_proyectos = TipoProyecto.all
    current_user
    current_grupo
    @proyecto = Proyecto.new(proyecto_params)
    @proyecto.idUsuario= @current_grupo
    if numeric(@proyecto.tipo)
    @proyecto.tipo=TipoProyecto.find(@proyecto.tipo).nombre
    end
    @proyecto.lineaCorte=1
    respond_to do |format|
      if @proyecto.save
        format.html { redirect_to @proyecto, notice: '' }
        format.json { render :show, status: :created, location: @proyecto }
      else
        format.html { render :new }
        format.json { render json: @proyecto.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /proyectos/1
  # PATCH/PUT /proyectos/1.json
  def update
     @tipo_proyectos = TipoProyecto.all
    current_user
    current_grupo
    respond_to do |format|
       proyecto=proyecto_params
      if numeric(proyecto[:tipo])
   proyecto[:tipo]=TipoProyecto.find(proyecto_params[:tipo])[:nombre]
   end
      if @proyecto.update(proyecto)
        format.html { redirect_to @proyecto, notice: '' }
        format.json { render :show, status: :ok, location: @proyecto }
      else
        format.html { render :edit }
        format.json { render json: @proyecto.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /proyectos/1
  # DELETE /proyectos/1.json
  def destroy
     @tipo_proyectos = TipoProyecto.all
    @proyecto.destroy
    current_user
    current_grupo
    respond_to do |format|
      format.html { redirect_to proyectos_url, notice: 'Proyecto was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
  

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_proyecto
       current_user
       current_grupo
      if !numeric(params[:id])
        redirect_to root_path
      else
        aux=0
        Grupo.all.where(:idUsuario=> @current_user["id"]).each do |grupo|
        if Proyecto.all.where(:idUsuario=>grupo.idGrupoUsuario).where(:id=>params[:id]).count>0
          @proyecto = Proyecto.find(params[:id])
          aux=1
      end
      end
      if aux==0
        redirect_to root_path
      end
      end
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def proyecto_params
      params.require(:proyecto).permit(:nombre, :tipo, :descripcion, :inicio, :fin, :idUsuario)
    end
end
