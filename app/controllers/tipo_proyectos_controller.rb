class TipoProyectosController < ApplicationController
 before_action :require_login
  before_action :set_tipo_proyecto, only: [:show, :edit, :update, :destroy]

  def require_login
     current_user
    unless @current_user!=nil
      redirect_to root_path
    end
  end
layout false
  # GET /tipo_proyectos
  # GET /tipo_proyectos.json
  def index
    current_user
    current_grupo
    @tipo_proyectos = TipoProyecto.all
  end

  # GET /tipo_proyectos/1
  # GET /tipo_proyectos/1.json
  def show
  current_user
  current_grupo
  end

  # GET /tipo_proyectos/new
  def new
    current_user
    current_grupo
    @tipo_proyecto = TipoProyecto.new
  end

  # GET /tipo_proyectos/1/edit
  def edit
    current_user
    current_grupo
  end

  # POST /tipo_proyectos
  # POST /tipo_proyectos.json
  def create
    current_user
    current_grupo
    @tipo_proyecto = TipoProyecto.new(tipo_proyecto_params)
    current_proyecto
    @tipo_proyecto.idUsuario=@current_grupo
    respond_to do |format|
      if @tipo_proyecto.save
        format.html { redirect_to new_tipo_proyecto_path}
        format.json { render :show, status: :created, location: @tipo_proyecto }
      else
        format.html { render :new }
        format.json { render json: @tipo_proyecto.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /tipo_proyectos/1
  # PATCH/PUT /tipo_proyectos/1.json
  def update
    current_user
    current_grupo
    respond_to do |format|
      if @tipo_proyecto.update(tipo_proyecto_params)
        format.html { redirect_to @tipo_proyecto, notice: '' }
        format.json { render :show, status: :ok, location: @tipo_proyecto }
      else
        format.html { render :edit }
        format.json { render json: @tipo_proyecto.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /tipo_proyectos/1
  # DELETE /tipo_proyectos/1.json
  def destroy
    current_user
    current_grupo
    @tipo_proyecto.destroy
    respond_to do |format|
      format.html { redirect_to tipo_proyectos_url, notice: '' }
      format.json { head :no_content }
    end
  end
  
   def numeric(s)
    Float(s) != nil rescue false
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_tipo_proyecto
      
       current_user
      if !numeric(params[:id])
        redirect_to root_path
      else
        aux=0
        Grupo.all.where(:idUsuario=> @current_user["id"]).each do |grupo|
        if TipoProyecto.all.where(:idUsuario=>grupo.idGrupoUsuario).where(:id=>params[:id]).count>0
          @tipo_proyecto = TipoProyecto.find(params[:id])
          aux=1
      end
      end
      if aux==0
        redirect_to root_path
      end
      end
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def tipo_proyecto_params
      params.require(:tipo_proyecto).permit(:nombre, :idUsuario)
    end
end
