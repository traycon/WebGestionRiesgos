json.array!(@grupos) do |grupo|
  json.extract! grupo, :id, :idUsuario, :idGrupoUsuario
  json.url grupo_url(grupo, format: :json)
end
