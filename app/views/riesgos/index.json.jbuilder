json.array!(@riesgos) do |riesgo|
  json.extract! riesgo, :id, :nombre, :categoria, :tipo, :impacto, :probabilidad, :descripcion, :factores, :reduccion, :supervision, :idProyecto, :idUsuario
  json.url riesgo_url(riesgo, format: :json)
end
