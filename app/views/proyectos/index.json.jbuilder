json.array!(@proyectos) do |proyecto|
  json.extract! proyecto, :id, :nombre, :tipo, :descripcion, :inicio, :fin, :idUsuario
  json.url proyecto_url(proyecto, format: :json)
end
