json.array!(@categoria_riesgos) do |categoria_riesgo|
  json.extract! categoria_riesgo, :id, :nombre, :idUsuario
  json.url categoria_riesgo_url(categoria_riesgo, format: :json)
end
