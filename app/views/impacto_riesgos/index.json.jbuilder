json.array!(@impacto_riesgos) do |impacto_riesgo|
  json.extract! impacto_riesgo, :id, :nombre, :idUsuario
  json.url impacto_riesgo_url(impacto_riesgo, format: :json)
end
