json.array!(@usuarios) do |usuario|
  json.extract! usuario, :id, :username, :password, :alias
  json.url usuario_url(usuario, format: :json)
end
