Rails.application.routes.draw do
  resources :grupos
  resources :riesgos
  resources :categoria_riesgos
  resources :impacto_riesgos
  resources :tipo_proyectos
  resources :proyectos
  resources :usuarios
  
  root 'pages#home'
  post 'login' => 'pages#login'
  post 'recuperarPass' => 'pages#recuperarPass'
  get 'recuperar' => 'pages#recuperar'
  get 'correo'=> 'pages#correo'
  get 'registrarse' => 'pages#registrar'
  get 'salir' => 'pages#salir'
  get 'delTipoProyecto/:id'=> 'tipo_proyectos#destroy', as: :delTipoProyecto
  get 'delCategoriaRiesgos/:id'=> 'categoria_riesgos#destroy', as: :delCategoriaRiesgos
  get 'delImpactoRiesgos/:id'=> 'impacto_riesgos#destroy', as: :delImpactoRiesgos
  get 'delRiesgo/:id'=> 'riesgos#destroy', as: :delRiesgo
  get 'delProyecto/:id'=> 'proyectos#destroy', as: :delProyecto
  get 'subir' => 'proyectos#lineaCorteUp'
  get 'bajar' => 'proyectos#lineaCorteDown'
  get 'estadisticas' => 'pages#estadisticas'
  get 'add/:id'=>'riesgos#add', as: :addRiesgo
  get 'nuevoProyecto/:id'=> 'proyectos#new', as: :nuevoProyecto
  get 'delGrupo/:id'=> 'grupos#destroy', as: :delGrupo
  get 'addUserToGroup/:id'=>'pages#addUserToGroup', as: :addUserToGroup
  post 'addtog'=> 'pages#addtog'
  get 'darPermisos/:igu/:iu'=> 'pages#darPermisos', as: :darPermisos
  get 'quitarPermisos/:igu/:iu'=> 'pages#quitarPermisos', as: :quitarPermisos
  get 'del_user_grupo/:igu/:iu' => 'pages#del_user_grupo', as: :del_user_grupo
  get '*path' => redirect('/')
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
